### Candidate Chinese Name:
* 
 邓易平
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 
思路：
	1、找出规律分解时间，抽象成不同的构成组件；
	2、通过定义职责明确的接口来规范和统一各组件的内部结构、赋予其统一行为（数据合法性检查、转换生成预期格式的字符串）；
	3、进一步分离时间灯组的生成方式，抽象成通用工具类，统一实现具体转换逻辑。

- - -