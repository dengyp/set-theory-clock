package com.paic.arch.interviews.constant;

/**
 * 常量类
 * 
 * @author dengyiping
 *
 */
public class Constant {
	
	public static final int ONE = 1;

	public static final int THREE = 3;
	
	public static final int FOUR = 4;
	
	public static final int FIVE = 5;
	
	public static final int ELEVEN = 11;
}
