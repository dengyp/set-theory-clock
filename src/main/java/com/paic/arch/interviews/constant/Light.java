package com.paic.arch.interviews.constant;

public enum Light {
	
	/**
	 * 默认亮灯
	 */
	DEFAULT("O", "默认"),
	
	/**
	 * 黄灯
	 */
	YELLOW("Y", "黄"), 
	
	/**
	 * 红灯
	 */
	RED("R", "红");
	
	private String symbol;
	
	private String desc;
	
	private Light(String symbol, String desc) {
		this.symbol = symbol;
		this.desc = desc;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getDesc() {
		return desc;
	}
	
}
