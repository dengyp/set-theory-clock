package com.paic.arch.interviews;

/**
 * 时间校验接口
 * 
 * @author dengyiping
 *
 */
public interface TimeValidation {
	
	boolean validateFormat(String timeValue) throws Exception;
}
