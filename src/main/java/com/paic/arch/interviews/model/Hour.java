package com.paic.arch.interviews.model;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.TimeValidation;
import com.paic.arch.interviews.constant.Constant;
import com.paic.arch.interviews.constant.Light;
import com.paic.arch.interviews.utils.TimeRowUtils;

/**
 * 组件：时
 * 
 * @author dengyiping
 *
 */
public class Hour implements TimeValidation, TimeConverter  {
	
	private static final int MAX_HOUR = 24;
	
	private int value ;

	/**
	 * hour should between [0, 24]
	 */
	@Override
	public boolean validateFormat(String timeValue) throws Exception {
		int tValue = Integer.parseInt(timeValue);
		if (tValue > Hour.MAX_HOUR) {
			return false;
		}
		
		if (tValue < 0) {
			return false;
		}
		
		return true;
	}

	/**
	 * covert hour of time
	 */
	public String convertTime(String aTime) throws Exception {
		this.value = Integer.parseInt(aTime);
		StringBuilder sb = new StringBuilder();
		sb.append(TimeRowUtils.generateTimeRow(this.value / Constant.FIVE , Constant.FOUR, Light.RED));
		sb.append(TimeRowUtils.generateTimeRow(this.value % Constant.FIVE , Constant.FOUR, Light.RED));
		return sb.toString();
	}

	

}
