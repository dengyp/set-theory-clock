package com.paic.arch.interviews.model;

import org.apache.commons.lang.StringUtils;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.TimeValidation;

/**
 * 时间类
 * 
 * @author dengyiping
 *
 */
public class Time implements TimeValidation, TimeConverter {
	private Hour hour;
	
	private Minute minute;
	
	private Second second;
 
	/**
	 * 
	 * @param timeValue HH:MM:SS
	 */
	public Time() {
		hour = new Hour();
		minute = new Minute();
		second = new Second();
	}

	/**
	 * 验证时间格式是否正确
	 * 
	 */
	@Override
	public boolean validateFormat(String timeValue) throws Exception {
		if (StringUtils.isEmpty(timeValue)) {
			return false;
		}
		
		String[] elements = timeValue.split(":");
		if (elements.length != 3) {
			return false;
		}
		
		return hour.validateFormat(elements[0]) && minute.validateFormat(elements[1])&& second.validateFormat(elements[2]);
	}

	/**
	 * 转换时间格式
	 */
	@Override
	public String convertTime(String aTime) throws Exception {
		if(!validateFormat(aTime)) {
			throw new Exception("时间校验失败，请检查");
		}
		String[] elements = aTime.split(":");
		
		/* 通过调用各时间组件格式化来实现*/
		StringBuilder sb = new StringBuilder();
		sb.append(second.convertTime(elements[2]));
		sb.append(hour.convertTime(elements[0]));
		sb.append(minute.convertTime(elements[1]));
		
		// 移除末尾的换行符
		String formatTime = sb.substring(0, sb.length()-2);
		return formatTime;
	}
	
	public static void main(String[] args) {
		try {
			Time time = new Time();
			System.out.println(time.convertTime("00:00:00"));
			System.out.println("#######");
			
			System.out.println(time.convertTime("13:17:01"));
			System.out.println("#######");
			
			System.out.println(time.convertTime("23:59:59"));
			System.out.println("#######");
			
			System.out.println(time.convertTime("24:00:00"));
			System.out.println("#######");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
