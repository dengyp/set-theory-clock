package com.paic.arch.interviews.model;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.TimeValidation;
import com.paic.arch.interviews.constant.Constant;
import com.paic.arch.interviews.constant.Light;
import com.paic.arch.interviews.utils.TimeRowUtils;

/**
 * 组件：分
 * 
 * @author dengyiping
 *
 */
public class Minute implements TimeValidation, TimeConverter {

	private static int MAX_MINUTE = 60;
	
	private int value ;

	/**
	 * minute should between [0, 60)
	 */
	@Override
	public boolean validateFormat(String timeValue) throws Exception {
		int tValue = Integer.parseInt(timeValue);
		if (tValue >= Minute.MAX_MINUTE) {
			return false;
		}
		
		if (tValue < 0) {
			return false;
		}
		
		return true;
	}

	/**
	 * covert minute of time
	 */
	@Override
	public String convertTime(String aTime) throws Exception {
		this.value = Integer.parseInt(aTime);
		StringBuilder sb = new StringBuilder();
		sb.append(TimeRowUtils.generateTimeRow(this.value / Constant.FIVE , Constant.ELEVEN, Constant.THREE, Light.YELLOW, Light.RED));
		sb.append(TimeRowUtils.generateTimeRow(this.value % Constant.FIVE , Constant.FOUR, Light.YELLOW));
		return sb.toString();
	}

}
