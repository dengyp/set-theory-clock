package com.paic.arch.interviews.model;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.TimeValidation;
import com.paic.arch.interviews.constant.Constant;
import com.paic.arch.interviews.constant.Light;
import com.paic.arch.interviews.utils.TimeRowUtils;

/**
 * 组件：秒
 * 
 * @author dengyiping
 *
 */
public class Second implements TimeValidation, TimeConverter {
	
	private static int MAX_SECOND = 60;
	
	private int value;

	/**
	 * second should between [0, 60)
	 */
	@Override
	public boolean validateFormat(String timeValue) throws Exception {
		int tValue = Integer.parseInt(timeValue);
		if (tValue >= Second.MAX_SECOND) {
			return false;
		}
		
		if (tValue < 0) {
			return false;
		}
		
		return true;
	}

	/**
	 * covert second of time
	 */
	@Override
	public String convertTime(String aTime) throws Exception {
		this.value = Integer.parseInt(aTime);
		StringBuilder sb = new StringBuilder();
		int length = 0;
		if (this.value % 2 == 0) {
			length = 1;
		}
		sb.append(TimeRowUtils.generateTimeRow(length, Constant.ONE, Light.YELLOW));
		return sb.toString();
	}


}
