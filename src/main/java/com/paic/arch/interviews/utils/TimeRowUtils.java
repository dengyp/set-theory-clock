package com.paic.arch.interviews.utils;

import com.paic.arch.interviews.constant.Light;

/**
 * 时间灯组生成工具类
 *
 * @author dengyiping
 *
 */
public class TimeRowUtils {
	
	/**
	 * 生成相应的灯组
	 * 
	 * @param length	亮灯数
	 * @param rowSize	行容量
	 * @param light		亮灯颜色
	 * @return
	 */
	public static String generateTimeRow(int length, int rowSize, Light light) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rowSize; i++) {
			if (i < length) {
				sb.append(light.getSymbol());
			} else {
				sb.append(Light.DEFAULT.getSymbol());
			}
		}
		
		sb.append("\r\n");
		return sb.toString();
	}
	
	/**
	 * 生成相应的灯组
	 * 
	 * @param length	亮灯数
	 * @param rowSize	行容量
	 * @param partition	分割位
	 * @param light		亮灯颜色
	 * @param partitionLight  分割位亮灯颜色
	 * @return
	 */
	public static String generateTimeRow(int length, int rowSize, int partition, Light light, Light partitionLight) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < rowSize; i++) {
			if (i % partition == 2 && i < length ) {
				sb.append(partitionLight.getSymbol());
			} else if (i < length) {
				sb.append(light.getSymbol());
			} else {
				sb.append(Light.DEFAULT.getSymbol());
			}
		}
		sb.append("\r\n");
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		String aa = TimeRowUtils.generateTimeRow(9, 4, Light.RED);
		System.out.println(aa);
		
		String bb = TimeRowUtils.generateTimeRow(11, 11, 3, Light.YELLOW, Light.RED);
		System.out.println(bb);
		
	}
}
